#pragma once

#include "proj.win32/pokemon/Pokemon.h"
#include "proj.win32/characters/Character.h"

class Trainer : public Character
{

public:
	Trainer(int id, std::string name, Sprite *sprite);
	Trainer(std::string name, Sprite *sprite);
	~Trainer();

	virtual void start();
	void addFootstep(float delta);
	void addPokemonToBelt(Pokemon pokemon);
	int getFootstep();
	static int getFMaxFootstep();
	vector<Pokemon> *getPokemonToBelt();

	virtual void update(float delta);

protected:
	void cameraFollowTrainer();

	int footstep;
	static int maxFootstep;
	vector<Pokemon> *pokemons;
	vector<Pokemon> *pokemonBelt;
};

