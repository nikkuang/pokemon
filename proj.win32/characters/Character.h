#pragma once

#include "cocos2d.h"
#include <iostream>

using namespace cocos2d;
using namespace std;

enum CHARACTER_DIRECTION
{
	TOP = 1,
	RIGHT = 2,
	DOWN = 3,
	LEFT = 4
};

class Character : public Node
{

public:
	Character(std::string name, Sprite *sprite);
	
	Character(int id, std::string name, Sprite *sprite);
	
	~Character();

	virtual void start();

	Sprite* getSprite();

	void setSprite(Sprite *sprite);

	void setIsMine(bool isMine);
	
	bool getIsMine();

	virtual void onKeyPressed(EventKeyboard::KeyCode keyCode, Event* event);

	virtual void onKeyReleased(EventKeyboard::KeyCode keyCode, Event* event);

	virtual void update(float delta);

	bool getIsMoving();

	Rect* getCollisionBox();

	CHARACTER_DIRECTION getDirection();

	void setDirection(CHARACTER_DIRECTION direction);

protected:


	/* PROPERTIES */
	int id;
	
	std::string name;
	
	Sprite *sprite;
	
	bool isMine;

	bool isMoving;

	bool canMove;

	float speed;

	CHARACTER_DIRECTION direction;

	//define the last position index for sprite animation
	int lastPosition;

	//the key index define if the player is already press more than 2 key on the keyboard
	int keyDirection;

	bool hasCollision;

	Rect* collisionBox;

	/* METHODS */
	void initEvents();

	void addMovement(float delta);

	void playMovementAnimation(float delta);

	virtual void debug();
};

