#include "Trainer.h"
#include "cocos2d.h"
#include <iostream>
#include "SimpleAudioEngine.h"
#include "proj.win32/maps/MapManager.h"
#include "proj.win32/tools/Debug.h"
#include "proj.win32/ui/UIManager.h"
#include "proj.win32/ui/BeltUI.h"

using namespace cocos2d;
using namespace std;

int Trainer::maxFootstep = 25;

#pragma region CONSTRUCTORS

Trainer::Trainer(int id, std::string name, Sprite *sprite) : Character(id, name, sprite)
{
	this->pokemons = new vector<Pokemon>;
	this->pokemonBelt = new vector<Pokemon>;

	this->start();
}

Trainer::Trainer(std::string name, Sprite *sprite) : Character(name, sprite)
{
	this->pokemons = new vector<Pokemon>;
	this->pokemonBelt = new vector<Pokemon>;
	
	this->start();
}

Trainer::~Trainer()
{
	delete(this->pokemonBelt);
	delete(this->pokemons);
}

#pragma endregion

#pragma region METHODS

void Trainer::start()
{
	this->footstep = 0;
	this->schedule(schedule_selector(Trainer::addFootstep), 0.5f);
}

void Trainer::update(float delta)
{
	this->cameraFollowTrainer();

	Character::update(delta);
}

void Trainer::addFootstep(float delta)
{
	if (this->isMoving)
	{
		if (this->footstep == maxFootstep)
			this->footstep = 0;

		this->footstep++;

		bool hasFighting = MapManager::checkFightForCharacter(this);
		if (hasFighting)
		{
			this->footstep = 0;
			CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic("sound/music/battle1.mp3", true);
			log("FIGHTING !!!!");
			this->canMove = false;
		}
		
		if (DEBUG_MODE == 1)
			log("Footstep => %i", this->footstep);
	}
}

void Trainer::addPokemonToBelt(Pokemon pokemon)
{
	//add pokemon to the belt if the belt is full then add it into the pokemons (computer)
	if (pokemonBelt->size() < 6)
		pokemonBelt->push_back(pokemon);
	else
		pokemons->push_back(pokemon);

	if (UIManager::getInstance())
	{
		//refresh ui belt
		auto uiBelt = static_cast<BeltUI*>(UIManager::getInstance()->getChildByName("pokemon_belt"));
		if (uiBelt)
			uiBelt->refreshBeltContent(this->pokemonBelt);	
	}

	if (DEBUG_MODE == 1)
	{
		std::string name = pokemon.getName();
		log("Pokemon added ! name => %s", name.c_str());
	}

}

void Trainer::cameraFollowTrainer()
{
	auto layer = Director::getInstance()->getRunningScene()->getChildByName("Game");
	auto tileMap = MapManager::getMapInfo();

	Size winSize = Director::getInstance()->getWinSize();

	Vec2 position = this->getPosition();

	int x = MAX(position.x, winSize.width / 2);
	int y = MAX(position.y, winSize.height / 2);
	x = MIN(x, (tileMap->getMapSize().width * tileMap->getTileSize().width) - winSize.width / 2);
	y = MIN(y, (tileMap->getMapSize().height * tileMap->getTileSize().height) - winSize.height / 2);
	Vec2 actualPosition = Vec2(x, y);

	Vec2 centerOfView = Vec2(winSize.width / 2, winSize.height / 2);
	Vec2 viewPoint = centerOfView - actualPosition;
	layer->setPosition(viewPoint);
}

#pragma endregion

#pragma region GETTERS/SETTERS

vector<Pokemon>* Trainer::getPokemonToBelt()
{
	return this->pokemonBelt;
}

int Trainer::getFootstep()
{
	return this->footstep;
}

int Trainer::getFMaxFootstep()
{
	return Trainer::maxFootstep;
}

#pragma endregion